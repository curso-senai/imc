calcular.addEventListener('click', function () {
    let calc;
    let inpname = nome.value;
    let inpaltura = altura.value;
    let inpsexMasc = sexoMascu.checked;
    let inpsexFem = sexoFemi.checked;

    if(inpsexMasc) calc = calcularIMC(inpaltura, 'M');
    else calc = calcularIMC(inpaltura, 'F')
    
    viewResult(calc, inpname);
})

function calcularIMC(heigth, sex){
    return sex === 'M' ? 135/ Math.pow(heigth, 2): 132 / Math.pow(heigth, 2);
}

function viewResult(calc, nome){
    pesoIdeal.innerHTML = `${Math.floor(calc)}kg`;
    nomeInformado.innerHTML = nome
    result.style.display = "flex";
}